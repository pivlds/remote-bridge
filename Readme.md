#remotebridge

Configuration tool for setting up SSH Port Tunnels in Java.

## Usage

The local and remote host must be ready to setup SSH connections via RSA keys.  

* Use the script `src/main/bash/ssh_setup.sh` to ensure key exchange.
  
Once the SSH keys are configured, create a `new RemoteBridge(Properties)` with the following properties:

* `ssh_host` : The ip or hostname of the SSH server
* `ssh_user` : The username for the SSH server

* `remote_host` : The hostname of the remote server
* `remote_port` : The port from the remote server to map

* `local_port` : (optional) The local port to map to `remote_host:remote_port`.   
`remote_port` will be used if this value is not set.


## Included Example

The configuration files shipped with the project maps the JMU Postgres db server `db.cs.jmu.edu:5432` to `localhost:5432` via `ssh://student.cs.jmu.edu`

To run the JMU DB bridge:

* Run the java file `RemoteJmuDb.java`

* You will be prompted for the SSH user name on first usage.

* Configuration will be saved to `~/jmudb`