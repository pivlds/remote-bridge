package edu.jmu.cs474.pivlds.remotebridge;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import javax.swing.JOptionPane;

public class RemoteJmuDb {

	public static void main(String[] args) throws Exception {
		jmuBridge();
	}

	private static final File CONFIG = new File(CommonFiles.USER_HOME, "jmudb");

	public static void jmuBridge() throws Exception {
		Properties props = new Properties();
		try {
			props.load(new FileInputStream(CONFIG));
		} catch (Exception e) {
			props.load(RemoteJmuDb.class.getResource("/jmudb").openStream());

			String user = JOptionPane.showInputDialog("Enter username for: " + props.getProperty("ssh_host"));
			props.put("ssh_user", user);
			props.store(new FileOutputStream(CONFIG), null);
		}
		RemoteBridge bridge = new RemoteBridge(props);
		bridge.setDebug(true);
		bridge.connect();

	}

}
