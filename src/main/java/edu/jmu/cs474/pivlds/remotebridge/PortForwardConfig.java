package edu.jmu.cs474.pivlds.remotebridge;

import java.util.Properties;

public class PortForwardConfig {

	Integer localPort;
	Integer remotePort;
	String remoteHost;

	public PortForwardConfig() {
	}

	public PortForwardConfig(Properties props) {
		readConfig(props);
	}

	public Integer getLocalPort() {
		return localPort == null ? remotePort : localPort;
	}

	public void setLocalPort(String intString) {
		setLocalPort(parseInt(intString));
	}

	public void setLocalPort(Integer localPort) {
		this.localPort = localPort;
	}

	public Integer getRemotePort() {
		return remotePort;
	}

	public void setRemotePort(String intString) {
		setRemotePort(parseInt(intString));
	}

	public void setRemotePort(Integer remotePort) {
		this.remotePort = remotePort;
	}

	public String getRemoteHost() {
		return remoteHost;
	}

	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}

	public void readConfig(Properties props) {
		setLocalPort(props.getProperty(PropertyKeys.LOCAL_PORT));
		setRemotePort(props.getProperty(PropertyKeys.REMOTE_PORT));
		setRemoteHost(props.getProperty(PropertyKeys.REMOTE_HOST));
	}

	private Integer parseInt(String intString) {
		if (intString == null || intString.length() == 0)
			return null;

		return Integer.parseInt(intString);
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(localPort);
		sb.append(":");
		sb.append(remoteHost);
		sb.append(":");
		sb.append(remotePort);
		return sb.toString();
	}

	public static interface PropertyKeys {
		public static final String LOCAL_PORT = "local_port";
		public static final String REMOTE_PORT = "remote_port";
		public static final String REMOTE_HOST = "remote_host";
	}

}
