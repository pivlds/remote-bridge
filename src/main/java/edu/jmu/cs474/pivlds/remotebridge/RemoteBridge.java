package edu.jmu.cs474.pivlds.remotebridge;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class RemoteBridge {

	boolean debug = false;

	Session session;

	String sshHost;
	String sshUser;

	List<PortForwardHandler> portForwardHandlers = new LinkedList<>();

	Exception lastError;

	public RemoteBridge(Properties props) {
		config(props);
	}

	public void add(PortForwardConfig pfc) {
		portForwardHandlers.add(new PortForwardHandler(pfc, this));
	}

	public void config(Properties props) {
		sshUser = props.getProperty(PropertyKeys.SSH_USER);
		sshHost = props.getProperty(PropertyKeys.SSH_HOST);

		add(new PortForwardConfig(props));
	}

	public boolean connect() {
		try {
			JSch jsch = new JSch();
			session = jsch.getSession(sshUser, sshHost);

			jsch.setKnownHosts(CommonFiles.KNOWN_HOSTS.getPath());
			jsch.addIdentity(CommonFiles.ID_RSA.getPath());

			session.connect();

			for (PortForwardHandler handler : portForwardHandlers) {
				handler.apply();
			}
		} catch (Exception e) {
			disconnect(e);
		}
		if (debug) {
			System.out.println(this);
		}
		return isConnected();
	}

	public void disconnect(Exception e) {
		disconnect();
		lastError = e;
		if (debug)
			lastError.printStackTrace();
	}

	public void disconnect() {
		if (isConnected()) {
			session.disconnect();
			session = null;
		}
	}

	public boolean isConnected() {
		return session != null && session.isConnected();
	}

	public String getConnectionString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(sshUser);
		sb.append("@");
		sb.append(sshHost);
		sb.append("](connected=");
		sb.append(isConnected());
		sb.append(")");
		return sb.toString();
	}

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public String getSshHost() {
		return sshHost;
	}

	public void setSshHost(String ssh_host) {
		this.sshHost = ssh_host;
	}

	public String getSshUser() {
		return sshUser;
	}

	public void setSshUser(String ssh_user) {
		this.sshUser = ssh_user;
	}

	public Session getSession() {
		return session;
	}

	public Exception getLastError() {
		return lastError;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getConnectionString());
		sb.append("{");
		for (int i = 0; i < portForwardHandlers.size(); i++) {
			if (i > 0)
				sb.append(", ");
			sb.append(portForwardHandlers.get(i));
		}
		sb.append("}");
		return sb.toString();
	}

	public interface PropertyKeys {
		public static final String SSH_USER = "ssh_user";
		public static final String SSH_HOST = "ssh_host";
	}

}
