package edu.jmu.cs474.pivlds.remotebridge;

public class PortForwardHandler {

	PortForwardConfig config;

	RemoteBridge bridge;

	public PortForwardHandler(PortForwardConfig config, RemoteBridge bridge) {
		this.config = config;
		this.bridge = bridge;
	}

	public boolean apply() {
		try {
			bridge.getSession().setPortForwardingL(config.getLocalPort(), config.getRemoteHost(),
					config.getRemotePort());
		} catch (Exception e) {
			bridge.disconnect(e);
			return false;
		}
		return true;
	}

	public boolean isApplied() {
		try {
			String[] configs = bridge.getSession().getPortForwardingL();
			for (String s : configs) {
				if (s.equals(config.toString()))
					return true;
			}
		} catch (Exception e) {
			bridge.disconnect(e);
		}
		return false;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(config.toString());
		sb.append("]");
		sb.append("(connected=" + isApplied());
		sb.append(")");
		return sb.toString();
	}

}
