package edu.jmu.cs474.pivlds.remotebridge;

import java.io.File;

public class CommonFiles {

	public static final File USER_HOME = new File(System.getProperty("user.home"));

	public static final File ID_RSA = new File(USER_HOME, "/.ssh/id_rsa");

	public static final File KNOWN_HOSTS = new File(USER_HOME, "/.ssh/known_hosts");

}
