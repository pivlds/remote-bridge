#!/bin/bash

# Sets up a remote ssh server as a known connection

# @param $1 the hostname for the ssh server
# @param $2 (optional) the username for the ssh server

echo .
echo Downloading remote SSH keys to local cache
echo .

bash ssh_scan.sh $1

echo .
echo Sending local SSH keys to server
echo .

bash send_key.sh $1 $2
