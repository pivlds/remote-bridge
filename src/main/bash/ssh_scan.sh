#!/bin/bash

# Ensures that the public ssh identities for a remote host are stored in the known_hosts cache.

#Remove the hosts from the known_hosts file
ssh-keygen -R $1

#Add all entries from the host.
ssh-keyscan -H $1 >> ~/.ssh/known_hosts
