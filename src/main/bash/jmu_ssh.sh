#!/bin/bash

# @param $1 the user name for the JMU Student server

# Does an SSH host scan for the jmu student server.

bash ssh_setup.sh "student.cs.jmu.edu" $1
