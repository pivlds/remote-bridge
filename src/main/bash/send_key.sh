#!/bin/bash

# Copies the id_rsa key to a remote server

# @param $1 the hostname for the ssh server
# @param $2 (optional) the username for the ssh server

if [ -z $2 ]; then
    ssh-copy-id $1
else
    ssh-copy-id $2@$1
fi

